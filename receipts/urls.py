from django.urls import path
from receipts.views import (
    get_receipt,
    create_receipt,
    get_category,
    get_account,
    create_category,
    create_account,
)

urlpatterns = [
    path("", get_receipt, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", get_category, name="get_category"),
    path("accounts/", get_account, name="get_account"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
