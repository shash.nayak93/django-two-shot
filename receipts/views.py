from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, CategoryForm, AccountForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required(login_url="/accounts/login/")
def get_receipt(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}

    return render(request, "receipts/home.html", context)


@login_required(login_url="/accounts/login/")
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {"create_form": form}

    return render(request, "receipts/create.html", context)


@login_required(login_url="/accounts/login/")
def get_category(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {"categories": categories}

    return render(request, "receipts/categories.html", context)


@login_required(login_url="/accounts/login/")
def get_account(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {"accounts": accounts}

    return render(request, "receipts/accounts.html", context)


@login_required(login_url="/accounts/login/")
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("get_category")
    else:
        form = CategoryForm()

    context = {"create_category_form": form}

    return render(request, "receipts/create_category.html", context)


@login_required(login_url="/accounts/login/")
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("get_account")
    else:
        form = AccountForm()

    context = {"create_account_form": form}

    return render(request, "receipts/create_account.html", context)
